# Exercices Ocaml

Liste d'exercices Ocaml, divisée en différentes catégories

A chaque exercice est associé un niveau de difficulté, allant de * à ***

Le but est de se restreindre dans chaque exercice à utiliser la spécificité de la catégorie
(ex : Dans fold_left_exos.ml, il faut résoudre chaque exercice à l'aide d'un fold_left)

Je recommande de faire les exercices dans l'ordre suivant :
1. [fold_left](exercices/fold_left_exos.ml)
2. [map](exercices/map_exos.ml)
3. [filter](exercices/filter_exos.ml)
4. [option](exercices/option_exos.ml)
5. [autres](exercices/autres_exos.ml) (hors-programme)



Si vous trouvez des coquilles, des erreurs, de meilleures manières de résoudre tel ou tel problème, n'hésitez pas à m'en faire part