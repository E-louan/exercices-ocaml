(*
  DEFINITION :
    let rec List.fold_left (f: 'a -> 'b -> 'a) (accu: 'a) (l: 'b list) :'a =
      match l with
      |[] -> accu
      |h::t -> List.fold_left f (f accu h) t

  EXPLICATION :
    List.fold_left permet d'executer une fonction sur chaque éléments d'une liste, en partant de la gauche,
    et avec un accumulateur
  
    List.fold_left f acc [e1 ; e2 ; ... ; en]
      -> f ( ... (f (f acc e1) e2 ) ... ) en
*)


let liste_premiers_entiers = List.init 20 (fun ind -> ind);;
let liste_ex = List.init 100 (fun i -> (Random.int 100));;
let phrase = ["H"; "e" ; "l" ; "l"; "o"; " "; "W" ; "o" ; "r" ; "l" ; "d"];; 
let phrase_codee = [("H", false); ("N", true ); ("j", false); ("d", false); ("o", false);
                    ("J", false); ("o", false); (" ", false); ("e", true ); ("D", false);
                    ("v", true ); ("e", true ); ("A", false); (" ", false); ("r", true );
                    (" ", true ); ("j", false); ("N", false); ("i", false); ("j", false);
                    ("g", true ); ("O", false); ("o", true ); ("n", true ); ("F", false);
                    (" ", false); ("Y", false); ("Q", false); ("P", false); ("n", true );
                    ("a", true ); ("t", false); (" ", true ); ("k", false); ("g", true );
                    ("L", false); ("i", true ); ("v", true ); ("e", true ); (" ", false);
                    ("o", false); ("r", false); (" ", true ); ("T", false); ("a", false);
                    ("Q", false); ("w", false); ("A", false); ("s", false); ("f", false);
                    (" ", false); ("W", false); ("d", false); ("y", true ); ("M", false);
                    ("o", true ); ("n", false); ("E", false); ("u", true ); ("p", false);
                    (" ", true ); ("1", false); ("-", false); ("u", true ); ("p", true )];; 

(* 1/ *
  ENTREE :
    l une liste d'entier
  RETOUR :
    la somme de tous les entiers présents dans la liste

  EXEMPLE :
    somme [1 ; 2 ; 3 ; 4 ; 32]
    -> 42
*)
let somme (l:int list) :int =
;;

somme liste_premiers_entiers;;


(* 2/ *
  ENTREE :
    l une liste d'entier
  RETOUR :
    la somme des carrés de tous les entiers présents dans la liste

  EXEMPLE :
    somme_carres [1 ; 2 ; 3 ; 4]
    -> 30
*)
let somme_carres (l:int list) :int =
;;

somme_carres liste_premiers_entiers;;


(* 3/ *
  ENTREE :
    l une liste de chaines de caractères
  RETOUR :
    la concaténation de toutes les chaines de caractères

  EXEMPLE :
    concat ["Hell" ; "o W" ; "orld"]
    -> "Hello World"
*)
let concat (l:string list) :string =
;;
concat phrase;;


(* 4/ **
  ENTREE :
    Une phrase codée sous forme d'une liste de couples (chaine de caractère, booléen)
  RETOUR :
    La phrase décodée, c'est à dire en concaténant les chaines de caractères
    appairées à true, et en ignorant celles associées à false

  EXEMPLE :
    decode [("Hello ", True) ; ("Mars", False) ; ("World", True)]
    -> Hello World
*)
let decode (l:(string*bool) list) :string =
;;

decode phrase_codee;;


(* 5/ *
  ENTREE :
    l une liste d'éléments de type 'a
  RETOUR :
    la liste contenant les mêmes éléments que l, mais dans l'autre sens

  EXEMPLE :
    rev [1 ; 2 ; 3 ; 4]
    -> [4 ; 3 ; 2 ; 1]
*)
let rev (l:'a list) :'a list =
;;

rev liste_premiers_entiers;;


(* 6/ **
  ENTREE :
    l une liste d'entier
  RETOUR :
    la liste contenant les mêmes entiers que l triés dans l'ordre croissant en
    utilisant l'algorithme du tri par insertion

  EXEMPLE :
    tri_insert [1 ; 4 ; 2 ; 3]
    -> [1 ; 2 ; 3 ; 4]
*)

let tri_insert (l: int list) :int list =
;;

tri_insert liste_ex;;


(* 7/ **
  ENTREE :
    l une liste d'entier
  RETOUR :
    la liste contenant les mêmes entiers que l triés dans l'ordre croissant en
    utilisant l'algorithme du tri rapide

  EXEMPLE :
    tri_rapide [1 ; 4 ; 2 ; 3]
    -> [1 ; 2 ; 3 ; 4]
  
  INDICATION (au bout de la ligne) ------------------------------------------------------------------------------------------------------------------------------------------------------------- fold_left peut servir à partitionner la liste
*)

let rec tri_rapide (l:int list) :int list =
;;

tri_rapide liste_ex;;
