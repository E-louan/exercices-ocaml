(*
  DEFINITION :
    let rec List.map (f: 'a -> 'b) (l: 'a list) =
      match l with
      |[] -> []
      |h::t -> (f h)::(List.map f t)

  EXPLICATION :
    List.map permet d'executer une fonction sur chaque élément d'une liste, et
    renvoie la liste ainsi obtenue
  
    List.map f [e1 ; e2 ; ... ; en]
      -> [f e1 ; f e2 ; ... ; f en]
*)


let liste_ex = List.init 100 (fun i -> (Random.int 100));;
let liste_premiers_entiers = List.init 20 (fun ind -> ind);;

(* 1/ *
  ENTREE :
    l une liste d'entier
  RETOUR :
    Une liste contenant les carrés des entiers de l

  EXEMPLE :
    carres [1 ; 2 ; 3 ; 4 ; 5]
    -> [1 ; 4 ; 9 ; 16 ; 25]
*)

let carres (l: int list) :int list =
;;

carres liste_premiers_entiers;;



(* 2/ *
  ENTREE :
    l une liste de listes d'éléments de type 'a
  RETOUR :
    Une liste contenant les longueurs de chacune des listes de l

  EXEMPLE :
    longueurs [[] ; [1 ; 2 ; 3] ; [42 ; 56] ; [1024 ; 1 ; 0 ; 0 ; 0]]
    -> [0 ; 3 ; 2 ; 5]
*)

let longueurs (l: 'a list list) :int list =
;;

longueurs [[78] ; [10 ; 23 ;  3] ; [42 ; 56] ; [1024 ; 1 ; 0] ; liste_ex ; liste_premiers_entiers];;

(* 3/ *
  ENTREE :
    l une liste de listes d'entiers
  RETOUR :
    La liste des listes de l, avec 0 rajoutés en tête de chacune d'elles

  EXEMPLE :
    ajoute_0 [[] ; [1 ; 2 ; 3] ; [42 ; 56] ; [1024 ; 1 ; 0 ; 0 ; 0]]
    -> [[0] ; [0 ; 1 ; 2 ; 3] ; [0 ; 42 ; 56] ; [0 ; 1024 ; 1 ; 0 ; 0 ; 0]]
*)

let ajoute_0 (l: int list list) :int list list =
;;

ajoute_0 [[] ; [1] ; [1 ; 2] ; [1 ; 2 ; 3]];;

(* 4/ ***
  ENTREE :
    l une liste d'éléments de type 'a
  RETOUR :
    La liste des préfixes de l

  EXEMPLE :
    prefixes [0 ; 1 ; 2 ; 3]
    -> [[] ; [0] ; [0 ; 1] ; [0 ; 1 ; 2] ; [0 ; 1 ; 2 ; 3]]
*)

let rec prefixes (l: 'a list) :'a list list =
;;

prefixes liste_premiers_entiers;;

