(*
  DEFINITION :
    type 'a option = None | Some of 'a

    let Option.get (e: 'a option) :'a = match e with
      |None -> raise Invalid_argument "option is None"
      |Some v -> v

    let Option.map (f: 'a -> 'b) (e: 'a option) :'b option = match e with
      |None -> None
      |Some v -> Some (f v)

  EXPLICATION :
    Le type 'a option permet de gérer l'absence potentielle d'un élément à
    calculer, en utilisant un filtrage, Option.get et Option.map
*)

let liste_ex = [None ; Some 3 ; None ; None ; Some 42 ; Some 51 ; Some 0 ; None];;

(* 1/ *
  ENTREE :
    l une liste d'entiers optionnels
  RETOUR :
    Une liste telle que chaque entier de l soit incrémenté

  EXEMPLE :
    incr_opt [None ; Some 0 ; Some 41 ; None]
    -> [None ; Some 1 ; Some 42 ; None]
*)

let incr_opt (l: int option list) :int option list =
;;

incr_opt liste_ex;;


(* 2/ *
  ENTREE :
    l une liste d'éléments de type 'a option
  RETOUR :
    La liste des valeurs non "None" de l

  EXEMPLE :
    filter [None ; Some 0 ; Some 42 ; None ; Some 1024]
    -> [0 ; 42 ; 1024]
*)

let rec filter (l: 'a option list) :'a list =
;;

filter liste_ex;;


(* 3/ **
  ENTREE :
    l une liste d'entiers optionnels
  RETOUR :
    Le minimum de la liste s'il y en a un

  EXEMPLE :
    min_opt [None ; Some 0 ; Some 41 ; None]
    -> Some 0

    min_opt [None ; None ; None]
    -> None

    min_opt []
    -> None
*)

let min_opt (l: int option list) :int option =
;;

min_opt liste_ex;;