(*
  DEFINITION :
    let rec List.filter (p: 'a -> bool) (l: 'a list) :'a list =
      match l with
      |[] -> []
      |h::t -> if p h then h::(List.filter p t) else (List.filter p t)

  EXPLICATION :
    List.filter est une fonction permettant de filtrer des éléments d'une liste

    List.filter p [e1 ; e2 ; ... ; en]
    -> [ei | 1 <= i <= n et (p ei)]
*)


let liste_ex = List.init 100 (fun i -> (Random.int 100));;
let seuil_ex = 50;;


(* 1/ *
  ENTREE :
    l une liste d'entier, s un seuil
  RETOUR :
    Une liste contenant les entiers de l plus petits que s 

  EXEMPLE :
    filtre_grands [1024 ; 2 ; 2 ; 4 ; 32 ; 42] 4
    -> [2 ; 2 ; 4]
*)
let filtre_grands (l: int list) (s: int) :int list =
;;

filtre_grands liste_ex seuil_ex;;


(* 2/ *
  ENTREE : l une liste de listes d'éléments de type 'a
  RETOUR : Une liste contenant uniquement les listes de longueur 1 présentes dans l

  EXEMPLE : singletons [[] ; [42] ; [27 ; 56] ; [1024]]
            -> [[42] ; [1024]]
*)
let singletons (l: 'a list list) :'a list list =
;;

singletons [[1 ; 2 ; 3] ; [5 ; 12] ; [578] ; [100 ; 790] ; [67]];;


(* 3/ **
  ENTREE :
    l une liste d'éléments de type 'a, p un prédicats sur les éléments de type 'a
  RETOUR :
    le nombre d'éléments de l vérifiant p

  EXEMPLE :
    count [1 ; 3 ; 4 ; 5 ; 42 ; 1024] est_pair    //où est_pair désigne une
    fonction testant la partié d'un entier
    -> 3
*)

let count (l: 'a list) (p: 'a -> bool) :int =
;;


let est_pair n = (n mod 2) = 0;;
count liste_ex est_pair;;
