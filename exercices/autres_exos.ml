(*
  DEFINITION APPLICATION PARTIELLE :
    si f est de type ('a -> 'b -> 'c), et ea est de type 'a,
    (f ea) est une fonction de type ('b -> 'c), et
    (f ea) = (fun eb -> f ea eb)
    L'application partielle est utile lorsque l'on utilise des fonctions
    d'ordre supérieur (fold_left, map, filter, etc)

  DEFINITION OPERATEURS INFIXES :
    Les opérateurs infixes (+ +. = <= <> / := etc.) sont juste des fonctions
    définies avec une certaine syntaxe, on peut donc en définir aussi.
    Pour ce faire, on écrit nos symboles entre parenthèses.
    Attention, seuls certains caractères sont autorisés :
    = < > | & $ @ ^ + - * / %
    De plus, selon son premier caractère, un opérateur appartient à l'un des
    cinq niveaux suivants, avec un ordre de priorité décroissant :
      0 : ( = < > | & $ ) -> associatif à gauche
      1 : ( @ ^ ) -> associatif à droite
      2 : ( + - ) -> associatif à gauche
      3 : ( * / % ) -> associatif à gauche
      4 : ( ** ) -> associatif à droite
    
    Concrètement, si (g) (resp. (d)) est un opérateur, associatif à
    gauche (resp. droite), voici comment sera interprété une expression :
      e1 g e2 g e3 -> (e1 g e2) g e3
      e1 d e2 d e3 -> e1 d (e2 d e3)

      Rq : equivalent à List.fold_left (g) <n> [e1 ; e2 ; e3]
           où <n> est l'élément neutre à gauche de (g)
           (resp. List.fold_right (d) <n> [e1 ; e2 ; e3], avec <n> elmt neutre
            à droite de (d))

  EXEMPLE :
    let (%) (f: 'a -> 'b) (g: 'b -> 'c) :'a -> 'c = fun x -> f (g x)
    désigne la composition mathématiques, et peut être utilisée de manière
    infixe
    (carre % plus_un) est alors la fonction qui à n associe (n+1)*(n+1)
 
    On peut aussi utiliser les opérateurs connus comme tels :
    4 + 5 est équivalent à (+) 4 5

    En combinant les opérateurs et l'application partielle, on peut construire
    ((+) 1), qui est équivalent à fun x -> (+) 1 x, et donc à fun x -> 1 + x
*)



let liste_premiers_entiers = List.init 20 (fun ind -> ind);;
let liste_ex = List.init 100 (fun i -> (Random.int 100));;
let seuil_ex = 50;;


(* 1/ *
  ENTREE :
    l une liste d'entier, s un seuil
  RETOUR :
    Une liste contenant les entiers de l plus petits que s

  EXEMPLE :
    filtre_grands [1024 ; 2 ; 2 ; 4 ; 32 ; 42] 4
    -> [2 ; 2 ; 4]
*)
let filtre_grands (l: int list) (s: int) :int list =
;;

filtre_grands liste_ex seuil_ex;;

(* 2/ *
  ENTREE :
    l une liste d'entiers
  RETOUR :
    La liste des entiers plus un
  
  EXEMPLE :
    ajoute_un [0 ; 1 ; 2 ; 3 ; 41]
    -> [1 ; 2 ; 3 ; 4 ; 42]
*)
let ajoute_un (l: int list) :int list =
;;

ajoute_un liste_premiers_entiers;;


(* 3/ ***
  ENTREE :
    l une liste d'entier
  RETOUR :
    la liste contenant les mêmes entiers que l triés dans l'ordre croissant
    en utilisant l'algorithme du tri rapide

  EXEMPLE :
    tri_rapide [1 ; 4 ; 2 ; 3]
    -> [1 ; 2 ; 3 ; 4]

  INDICATION :
    On pourra utiliser List.partition, dont la spécification est disponible
    sur la documentation Ocaml :)
*)
let rec tri_rapide (l:int list) :int list =
;;

tri_rapide liste_ex;;


(* 2/ **
  QUESTION : Sans executer le code, indiquez ce que chaque ligne va renvoyer
*)
let (^+) (s1: string) (s2: string) :string = s1 ^ s2 ^ s2;;
let (+^) (s1: string) (s2: string) :string = s1 ^ s2 ^ s2;;

(* A/ *)
"a" +^ "b" +^ "c";;

(* B/ *)
"a" ^+ "b" ^+ "c";;

(* C/ *)
"a" +^ "b" ^+ "c";;

(* D/ *)
"a" ^+ "b" +^ "c";;


(*
  Pour continuer, regardez le module List d'OCaml
  (https://v2.ocaml.org/api/List.html ou en cherchant "Ocaml List" sur internet)
  puis pour chaque fonction à partir de la partie "Iterators"
  jusqu'à "List of pairs" inclues :
    - L'utiliser dans un exemple pas complètement bidon
    - Essayer de la réimplémenter, si possible avec des fonctions vues précédemment

  Puis, plusieurs possibilités :
    - Aller voir le code de la librairie standard d'OCaml disponible sur Github
      (https://github.com/ocaml/ocaml/tree/trunk/stdlib)
    - Faire le même exercice que précédemment sur d'autres modules de la librairie
      standard : Array, Option, Either, Fun, Stdlib
*)